package com.ustcsse.parkinglot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ustcsse.parkinglot.model.po.Parkinglot;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ustcsse
 */
@Mapper
public interface ParkinglotMapper extends BaseMapper<Parkinglot> {

}
