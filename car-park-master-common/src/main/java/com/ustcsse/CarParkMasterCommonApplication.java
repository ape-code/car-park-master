package com.ustcsse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@SpringBootApplication
//@EnableDiscoveryClient
public class CarParkMasterCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarParkMasterCommonApplication.class, args);
    }

}
