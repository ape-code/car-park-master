package com.ustcsse.parking.service;

public interface NotifyService {
    public void receiveMessage(String message);
}
