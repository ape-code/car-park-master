package com.ustcsse.parking.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ustcsse.parking.model.po.Parking;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ustcsse
 */
@Mapper
public interface ParkingMapper extends BaseMapper<Parking> {

}
