package com.ustcsse.parking.mapper;

import com.ustcsse.parking.model.po.Parking;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ustcsse
 */
public interface ParkingMapper extends BaseMapper<Parking> {

}
