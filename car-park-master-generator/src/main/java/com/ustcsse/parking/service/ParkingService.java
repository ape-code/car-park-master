package com.ustcsse.parking.service;

import com.ustcsse.parking.model.po.Parking;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ustcsse
 * @since 2024-03-28
 */
public interface ParkingService extends IService<Parking> {

}
