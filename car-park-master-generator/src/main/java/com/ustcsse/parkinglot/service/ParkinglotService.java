package com.ustcsse.parkinglot.service;

import com.ustcsse.parkinglot.model.po.Parkinglot;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ustcsse
 * @since 2024-03-28
 */
public interface ParkinglotService extends IService<Parkinglot> {

}
