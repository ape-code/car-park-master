package com.ustcsse.parkinglot.mapper;

import com.ustcsse.parkinglot.model.po.Parkinglot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ustcsse
 */
public interface ParkinglotMapper extends BaseMapper<Parkinglot> {

}
