package com.ustcsse.auth.service;

import com.ustcsse.auth.model.po.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ustcsse
 * @since 2024-04-05
 */
public interface UserService extends IService<User> {

}
