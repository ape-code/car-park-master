package com.ustcsse.order.service;

import com.ustcsse.order.model.po.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ustcsse
 * @since 2024-03-28
 */
public interface OrderService extends IService<Order> {

}
