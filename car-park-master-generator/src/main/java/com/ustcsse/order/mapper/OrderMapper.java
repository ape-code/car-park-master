package com.ustcsse.order.mapper;

import com.ustcsse.order.model.po.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ustcsse
 */
public interface OrderMapper extends BaseMapper<Order> {

}
