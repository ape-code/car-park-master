package com.ustcsse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarParkMasterGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarParkMasterGeneratorApplication.class, args);
    }

}
