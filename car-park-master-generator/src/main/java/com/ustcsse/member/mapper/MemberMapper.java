package com.ustcsse.member.mapper;

import com.ustcsse.member.model.po.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ustcsse
 */
public interface MemberMapper extends BaseMapper<Member> {

}
