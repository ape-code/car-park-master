package com.ustcsse.member.service;

import com.ustcsse.member.model.po.Member;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ustcsse
 * @since 2024-03-28
 */
public interface MemberService extends IService<Member> {

}
