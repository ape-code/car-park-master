package com.ustcsse.user.mapper;

import com.ustcsse.user.model.po.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ustcsse
 */
public interface UserMapper extends BaseMapper<User> {

}
