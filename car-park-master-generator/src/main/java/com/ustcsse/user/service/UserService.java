package com.ustcsse.user.service;

import com.ustcsse.user.model.po.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ustcsse
 * @since 2024-03-28
 */
public interface UserService extends IService<User> {

}
